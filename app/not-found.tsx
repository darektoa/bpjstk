function NotFound() {
    return (
        <div className="col-span-9 flex h-full w-full items-center justify-center">
            <p className="text-xl text-gray-400 md:text-2xl">
                404 | Page Not Found
            </p>
        </div>
    );
}

export default NotFound;
