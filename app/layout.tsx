import './globals.css';
import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import App from '@/components/App';
import MUIThemeRegistry from '@/components/MUIThemeRegistry';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
    title: 'BPJSTK',
    description: 'Summary of BPJSTK Data',
};

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <html lang="en">
            <body className={inter.className}>
                <MUIThemeRegistry>
                    <App.Header />
                    <App.Content>{children}</App.Content>
                    {/* <App.Footer /> */}
                </MUIThemeRegistry>
            </body>
        </html>
    );
}
