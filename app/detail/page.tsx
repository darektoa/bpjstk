'use client';
import Link from 'next/link';

function Page() {
    return (
        <main className="col-span-9 mt-14">
            <header className="mb-6 flex w-full">
                <Link
                    href="/summary"
                    className="mr-4 flex border border-primary-main px-6 py-1 text-primary-main shadow-none rounded-lg"
                >
                    Summary
                </Link>
                <Link
                    href="/detail"
                    className="mr-4 flex border bg-primary-main px-6 py-1 text-white shadow-none rounded-lg"
                >
                    Detail Data
                </Link>
            </header>

            <div className="mb-6 flex w-full">
                <div className="mr-4 flex rounded-lg border border-gray-300 px-4 py-2">
                    <label
                        htmlFor="scopeDataSelect"
                        className="mr-3"
                    >
                        Kantor Wilayah
                    </label>
                    <select
                        name=""
                        id="scopeDataSelect"
                        className="font-bold text-primary-main"
                    >
                        <option value="kepesertaan">903 - DKI Jakarta</option>
                    </select>
                </div>
                <div className="mr-4 flex rounded-lg border border-gray-300 px-4 py-2">
                    <label
                        htmlFor="scopeDataSelect"
                        className="mr-3"
                    >
                        Wilayah
                    </label>
                    <select
                        name=""
                        id="scopeDataSelect"
                        className="font-bold text-primary-main"
                    >
                        <option value="kepesertaan">Jakarta Pusat</option>
                    </select>
                </div>
                <div className="mr-4 flex rounded-lg border border-gray-300 px-4 py-2">
                    <label
                        htmlFor="scopeDataSelect"
                        className="mr-3"
                    >
                        Cabang
                    </label>
                    <select
                        name=""
                        id="scopeDataSelect"
                        className="font-bold text-primary-main"
                    >
                        <option value="kepesertaan">Cilandak</option>
                    </select>
                </div>
                <div className="mr-4 flex rounded-lg border border-gray-300 px-4 py-2">
                    <label
                        htmlFor="scopeDataSelect"
                        className="mr-3"
                    >
                        Pembina
                    </label>
                    <select
                        name=""
                        id="scopeDataSelect"
                        className="font-bold text-primary-main"
                    >
                        <option value="kepesertaan">ALL</option>
                    </select>
                </div>
            </div>

            <section className="mb-6 flex w-full flex-col rounded-lg border p-4">
                <header className="mb-4 flex w-full">
                    <h3 className="text-xl font-bold">Grade & Rating</h3>
                    <p className="text-xl text-gray-500">
                        &nbsp;- Kepesertaan, Completeness
                    </p>
                </header>

                <div className="mb-10 h-52 w-full"></div>
            </section>

            <section className="mb-6 flex w-full flex-col rounded-lg border p-4">
                <header className="mb-4 flex w-full">
                    <h3 className="text-xl font-bold">Detail Column Mandatori</h3>
                    <p className="text-xl text-gray-500">
                        &nbsp;- Kepesertaan, Completeness
                    </p>
                </header>

                <div className="mb-10 h-52 w-full"></div>
            </section>

            <section className="mb-6 flex w-full flex-col rounded-lg border p-4">
                <header className="mb-4 flex w-full">
                    <h3 className="text-xl font-bold">Detail All Column</h3>
                    <p className="text-xl text-gray-500">
                        &nbsp;- Kepesertaan, Completeness
                    </p>
                </header>

                <div className="mb-10 h-52 w-full"></div>
            </section>

            <section className="mb-6 flex w-full flex-col rounded-lg border p-4">
                <header className="mb-4 flex w-full">
                    <h3 className="text-xl font-bold">Detail Pattern</h3>
                    <p className="text-xl text-gray-500">
                        &nbsp;- Kepesertaan, Completeness
                    </p>
                </header>

                <div className="mb-10 h-52 w-full"></div>
            </section>
        </main>
    );
}

export default Page;
