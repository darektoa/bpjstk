const config = ({ data, sizes }) => ({
    accessibility: {
        enabled: false,
    },
    chart: {
        type: "column",
        height: sizes?.height,
        width: sizes?.width,
        margin: [null, 20, null, null],
        events: {
            load: chartLoadHandle,
            redraw: chartLoadHandle,
        },
    },
    colors: [
        {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, 'rgba(81, 163, 255, 0.7)'],
                [1, '#386BA5']
            ]
        },
        {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, 'rgba(24, 146, 195, 0.7)'],
                [1, '#1892C3']
            ]
        },
        {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, 'rgba(48, 192, 218, 0.7)'],
                [1, '#30C0DA']
            ]
        },
        {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, 'rgba(79, 227, 193, 0.7)'],
                [1, '#4FE3C1']
            ]
        },
        {
            type: "spline",
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, '#3D4A77'],
                [1, '#3D4A77']
            ]
        },
    ],
    credits: {
        text: "",
    },
    legend: {
        // width: "100%",
        // align: "center",
        alignColumns: false,
        layout: "horizontal",
        itemDistance: 20,
        itemStyle: {
            margin: [0, "auto"],
            textOverflow: null,
        },
    },
    plotOptions: {
        column: {
            // pointWidth: sizes?.width / 10,
            pointPadding: 0.2,
            borderRadius: 6,
            borderWidth: 0,
        },
        spline: {
            marker: {
                symbol: "circle",
            },
        },
        series: {
            tooltip: {
                valueDecimals: 2,
                valueSuffix: "%",
            }
        }
    },
    title: {
        text: "",
    },
    // tooltip: {
    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //         '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    //     footerFormat: '</table>',
    //     shared: true,
    //     useHTML: true
    // },
    xAxis: {
        categories: data?.[0]?.map((statistic) => parseInt(statistic.year)),
        crosshair: true,
        labels: {
            style: {
                fontSize: "12px",
            },
        },
    },
    yAxis: {
        labels: {
            format: "{value}%",
            style: {
                fontSize: "12px",
            },
        },
        title: {
            text: "",
        },
    },
    series: data?.map((statistic, index) => ({
        type:
            statistic[0].region === "World" && data.length > 1
                ? "spline"
                : null,
        name: statistic[0].region,
        data: statistic?.map((item) => Number(item.value)),
        colorIndex: index,
    })),
});

export default config;
