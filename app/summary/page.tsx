'use client';
import Link from 'next/link';

function Page() {
    return (
        <main className="col-span-9 mt-14">
            <header className="mb-6 flex w-full">
                <Link
                    href="/summary"
                    className="px-6 mr-4 flex border bg-primary-main py-1 text-white shadow-none rounded-lg"
                >
                    Summary
                </Link>
                <Link
                    href="/detail"
                    className="px-6 mr-4 flex border border-primary-main py-1 text-primary-main shadow-none rounded-lg"
                >
                    Detail Data
                </Link>
            </header>

            <section className="flex w-full flex-col rounded-lg border p-4">
                <header className="mb-4 flex w-full">
                    <h3 className="text-xl font-bold">Kanwil DQI Score</h3>
                    <p className="text-xl text-gray-500">
                        &nbsp;- Kepesertaan, Completeness
                    </p>
                </header>

                <div className="mb-10 h-52 w-full"></div>

                <table className="w-full">
                    <thead className="w-full">
                        <tr>
                            <th className="bg-gray-300 px-3 py-4 text-left">
                                Kode - Kanwil
                            </th>
                            <th className="bg-gray-300 px-3 py-4 text-left">
                                Completeness
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                        <tr className="border-b border-gray-400">
                            <td className="p-3">901 - Banten</td>
                            <td className="p-3">93.00%</td>
                        </tr>
                    </tbody>
                </table>
            </section>
        </main>
    );
}

export default Page;
