export type ActionType<T> = T & {
    type?: string;
};

export type ContextProviderProps = {
    children: React.ReactNode;
};

export type ContextProviderReturn = JSX.Element;

export type ContextProviderType = (
    props: ContextProviderProps,
) => ContextProviderReturn;

export type ActionFunction<ContextT, ActionT> = (
    state: ContextT,
    action: ActionT,
) => ContextT;
