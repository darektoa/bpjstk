const upper = (text) => text.toUpperCase();

export default upper;
