FROM node:20.10.0

WORKDIR /app

COPY . .
RUN npm install
RUN npm install serve@latest

RUN npm run build

CMD ["npx", "serve@latest", "out", "-l", "3000"]
