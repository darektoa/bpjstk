import './style.css';

function Header() {
    return (
        <header className="app__header border-b border-gray-300">
            <div className="container">
                <h1>Dashboard Data Quality Assesment</h1>
            </div>
        </header>
    );
}

export default Header;
