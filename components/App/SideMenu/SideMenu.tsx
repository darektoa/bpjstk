import './style.css';
import { FeatherIcon } from '@/components/Icon';
import NavLink from '@/components/NavLink';
import PercentageBar from '@/components/PercentageBar';

function SideMenu() {
    return (
        <nav className="app__side-menu">
            <div className="mb-4 flex rounded-lg border border-gray-300 px-4 py-2">
                <label
                    htmlFor="scopeDataSelect"
                    className="mr-3"
                >
                    Scope Data
                </label>
                <select
                    name=""
                    id="scopeDataSelect"
                    className="font-bold text-primary-main"
                >
                    <option value="kepesertaan">Kepesertaan</option>
                </select>
            </div>

            <ul className="flex w-full flex-col">
                <li className="mb-4 flex w-full">
                    <section className="flex flex-wrap items-start rounded-lg border border-gray-300 p-4">
                        <h3 className="mb-3 w-full text-xl font-bold">
                            Nasional TK
                        </h3>
                        <figure className="mr-6 flex items-center rounded-full bg-sky-200 p-4">
                            <FeatherIcon.Users className="h-6 w-6 fill-primary-accent stroke-primary-accent" />
                        </figure>
                        <div className="flex grow flex-col items-start justify-center">
                            <p className="col-span-8 mb-1 text-base font-bold">
                                89,48%
                            </p>
                            <p className="col-span-8 text-sm text-gray-500">
                                70.635.000 records
                            </p>
                        </div>
                    </section>
                </li>
                <li className="mb-4 flex w-full">
                    <section className="flex flex-wrap items-start rounded-lg border border-gray-300 p-4">
                        <h3 className="mb-3 w-full text-xl font-bold">
                            Nasional TK Aktif
                        </h3>
                        <figure className="mr-6 flex items-center rounded-full bg-green-100 p-4">
                            <FeatherIcon.Users className="h-6 w-6 fill-green-500 stroke-green-500" />
                        </figure>
                        <div className="flex grow flex-col items-start justify-center">
                            <p className="col-span-8 mb-1 text-base font-bold">
                                89,75%
                            </p>
                            <p className="col-span-8 text-sm text-gray-500">
                                64.594.316 records
                            </p>
                        </div>
                    </section>
                </li>
                <li className="mb-4 flex w-full">
                    <section className="flex flex-wrap items-start rounded-lg border border-gray-300 p-4">
                        <h3 className="mb-3 w-full text-xl font-bold">
                            Nasional TK Non Aktif
                        </h3>
                        <figure className="mr-6 flex items-center rounded-full bg-red-100 p-4">
                            <FeatherIcon.Users className="h-6 w-6 fill-red-500 stroke-red-500" />
                        </figure>
                        <div className="flex grow flex-col items-start justify-center">
                            <p className="col-span-8 mb-1 text-base font-bold">
                                79,25%
                            </p>
                            <p className="col-span-8 text-sm text-gray-500">
                                6.040.638 records
                            </p>
                        </div>
                    </section>
                </li>
            </ul>

            <section className="flex w-full flex-col p-4">
                <h3 className="mb-3 text-xl font-bold">Dimension</h3>
                <ul>
                    <li className="flex w-full">
                        <section className="mb-3 flex flex-wrap items-start border-b border-gray-300 px-2 py-3 transition-colors duration-200 hover:rounded-lg hover:border hover:border-green-500 hover:bg-green-100">
                            <h3 className="mb-2 w-full text-base font-bold">
                                Completeness
                            </h3>
                            <div className="flex w-full items-center">
                                <PercentageBar
                                    percentage="80%"
                                    className="bg-green-500"
                                    wrapperClassName="w-44 h-2.5 mr-3"
                                />
                                <p className="text-sm font-bold">79,25%</p>
                                <FeatherIcon.ChevronRight className="ml-auto h-5 w-5 stroke-green-500" />
                            </div>
                        </section>
                    </li>
                    <li className="flex w-full">
                        <section className="mb-3 flex flex-wrap items-start border-b border-gray-300 px-2 py-3 transition-colors duration-200 hover:rounded-lg hover:border hover:border-green-500 hover:bg-green-100">
                            <h3 className="mb-2 w-full text-base font-bold">
                                Uniqueness
                            </h3>
                            <div className="flex w-full items-center">
                                <PercentageBar
                                    percentage="80%"
                                    className="bg-green-500"
                                    wrapperClassName="w-44 h-2.5 mr-3"
                                />
                                <p className="text-sm font-bold">79,25%</p>
                                <FeatherIcon.ChevronRight className="ml-auto h-5 w-5 stroke-green-500" />
                            </div>
                        </section>
                    </li>
                    <li className="flex w-full">
                        <section className="mb-3 flex flex-wrap items-start border-b border-gray-300 px-2 py-3 transition-colors duration-200 hover:rounded-lg hover:border hover:border-green-500 hover:bg-green-100">
                            <h3 className="mb-2 w-full text-base font-bold">
                                Validity
                            </h3>
                            <div className="flex w-full items-center">
                                <PercentageBar
                                    percentage="80%"
                                    className="bg-green-500"
                                    wrapperClassName="w-44 h-2.5 mr-3"
                                />
                                <p className="text-sm font-bold">79,25%</p>
                                <FeatherIcon.ChevronRight className="ml-auto h-5 w-5 stroke-green-500" />
                            </div>
                        </section>
                    </li>
                    <li className="flex w-full">
                        <section className="mb-3 flex flex-wrap items-start border-b border-gray-300 px-2 py-3 transition-colors duration-200 hover:rounded-lg hover:border hover:border-green-500 hover:bg-green-100">
                            <h3 className="mb-2 w-full text-base font-bold">
                                Accuracy
                            </h3>
                            <div className="flex w-full items-center">
                                <PercentageBar
                                    percentage="80%"
                                    className="bg-green-500"
                                    wrapperClassName="w-44 h-2.5 mr-3"
                                />
                                <p className="text-sm font-bold">79,25%</p>
                                <FeatherIcon.ChevronRight className="ml-auto h-5 w-5 stroke-green-500" />
                            </div>
                        </section>
                    </li>
                    <li className="flex w-full">
                        <section className="mb-3 flex flex-wrap items-start border-b border-gray-300 px-2 py-3 transition-colors duration-200 hover:rounded-lg hover:border hover:border-green-500 hover:bg-green-100">
                            <h3 className="mb-2 w-full text-base font-bold">
                                Consistency
                            </h3>
                            <div className="flex w-full items-center">
                                <PercentageBar
                                    percentage="80%"
                                    className="bg-green-500"
                                    wrapperClassName="w-44 h-2.5 mr-3"
                                />
                                <p className="text-sm font-bold">79,25%</p>
                                <FeatherIcon.ChevronRight className="ml-auto h-5 w-5 stroke-green-500" />
                            </div>
                        </section>
                    </li>
                </ul>
            </section>

            {/* <ul className="app__side-menu__list">
                <li className="app__side-menu__list__item">
                    <NavLink href="/summary">Summary</NavLink>
                </li>
                <li className="app__side-menu__list__item">
                    <NavLink href="/customer/segmentation">Customer Segmentation</NavLink>
                </li>
                <li className="app__side-menu__list__item">
                    <NavLink href="/customer/credit-score">Customer Credit Score</NavLink>
                </li>
            </ul> */}
        </nav>
    );
}

export default SideMenu;
