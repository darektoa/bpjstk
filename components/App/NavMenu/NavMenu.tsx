import './style.css';
import NavLink from '@/components/NavLink';

function NavMenu() {
    return (
        <nav className="app__nav-menu">
            <ul className="app__nav-menu__list">
                <li className="app__nav-menu__list__item">
                    <NavLink href="/summary">Summary</NavLink>
                </li>
                <li className="app__nav-menu__list__item">
                    <NavLink href="/customer/segmentation">Customer Segmentation</NavLink>
                </li>
                <li className="app__nav-menu__list__item">
                    <NavLink href="/customer/credit-score">Customer Credit Score</NavLink>
                </li>
            </ul>
        </nav>
    )
};

export default NavMenu;