import Content from './Content';
import Footer from './Footer';
import Header from './Header';
import NavMenu from './NavMenu';

const App = {
    Content,
    Footer,
    Header,
    NavMenu,
};

export default App;
