import './style.css';
import { PropsWithChildren } from 'react';
import SideMenu from '../SideMenu';

function Content({ children }: PropsWithChildren)
{
    return (
        <div className="app__content container">
            <SideMenu />
            <div className="app__content__main">
                { children }
            </div>
        </div>
    );
}

export default Content;