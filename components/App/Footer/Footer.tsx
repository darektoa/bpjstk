import './style.css';
import NavMenu from './NavMenu';

function Footer() {
    return (
        <footer className="app__footer">
            <div className="container">
                <p className="app__footer__copyright">
                    Hak Cipta © 2023 Xeratic
                </p>
                <NavMenu />
            </div>
        </footer>
    );
}

export default Footer;
