import { AnchorHTMLAttributes } from 'react';
import { LinkProps } from 'next/link';

export type NavLinkProps = LinkProps & AnchorHTMLAttributes<HTMLAnchorElement>;
export type NavLinkReturn = JSX.Element;