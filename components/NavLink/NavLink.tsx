'use client';
import './style.css';
import { usePathname } from 'next/navigation';
import { NavLinkProps, NavLinkReturn } from './types';
import Link from 'next/link';
import Str from '@/utilities/StringHelper';

function NavLink(props: NavLinkProps): NavLinkReturn {
    const { children, className, ...attrs } = props;
    const pathname = usePathname();
    const isActive = pathname === attrs.href;

    return (
        <Link
            {...attrs}
            className={Str.joinClassName(className, isActive ? 'active' : '')}
        >
            {children}
        </Link>
    );
}

export default NavLink;
