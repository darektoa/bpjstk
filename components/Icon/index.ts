import Icon from "./Icon";
import FeatherIcon from "./FeatherIcon";

Icon.FeatherIcon = FeatherIcon;

export {
    FeatherIcon
};

export default Icon;