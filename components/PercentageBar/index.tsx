import './style.css';
import { PercentageBarProps } from './types';
import Str from '@/utilities/StringHelper';

function PercentageBar({
    children,
    percentage,
    className,
    wrapperClassName,
}: PercentageBarProps) {
    return (
        <div
            className={Str.joinClassName(
                'percentage-bar-component',
                wrapperClassName,
            )}
        >
            <div
                style={{ width: percentage }}
                className={Str.joinClassName(
                    'percentage-bar-component__bar',
                    className,
                )}
            >
                {children}
            </div>
        </div>
    );
}

export default PercentageBar;
